using UnityEngine;
using System.Collections;

public class VisualHeartBeat : MonoBehaviour {
	
    public Texture2D heartbeatSmall;
	public Texture2D heartbeatBig;
	Texture2D currentTexture;
	float timePassedSinceLastBeat;
	public float heartBeatSpeed; // speed the heart beats in seconds
	bool heartBeatPumpingBig;
	
	public bool m_isBig = false;
	
	// Use this for initialization
	void Start () 
    {
		timePassedSinceLastBeat = 0;
		heartBeatPumpingBig = false;
		currentTexture = heartbeatBig;
	}
	
	// Update is called once per frame
	void Update () 
    {
  		
	}

    void OnGUI() 
    {
		if( m_isBig )
		{
			heartBeatPumpingBig = true;
			currentTexture = heartbeatBig;
			timePassedSinceLastBeat = 0;
			
		}
		else
		{
			heartBeatPumpingBig = false;
			currentTexture = heartbeatSmall;
			timePassedSinceLastBeat = 0;
		}
		
        /*timePassedSinceLastBeat += Time.deltaTime;
		if (heartBeatPumpingBig && timePassedSinceLastBeat >= heartBeatSpeed) {
			heartBeatPumpingBig = false;
			currentTexture = heartbeatSmall;
			timePassedSinceLastBeat = 0;
		}
		if (!heartBeatPumpingBig && timePassedSinceLastBeat >= heartBeatSpeed * 0.3f) {
			heartBeatPumpingBig = true;
			currentTexture = heartbeatBig;
			timePassedSinceLastBeat = 0;
		}*/
		GUI.BeginGroup(new Rect(Screen.width * 0.8f, Screen.height * 0.03f, Screen.height * 0.3f, Screen.height * 0.15f));
		GUI.DrawTexture(new Rect(0, 0, Screen.width * 0.1f, Screen.height * 0.15f), currentTexture);
		GUI.EndGroup();
    }
}
