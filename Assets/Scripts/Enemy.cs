using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour 
{
	public GUIText m_debugText;
	public float m_followDist = 5.0f;

    public AudioClip footSteps;

	public Transform m_hand;
	public bool m_isDead;
	
	private NavMeshAgent m_navMeshAgent;
	private Vector3 m_startPos;
	private Transform m_target;
    private Animator m_animator;
	
	private Vector3 m_lastPos;
	private bool m_isMoving = false;
	Health m_healthBar;
	private float m_lastHitTime = 0.0f;
	private bool m_playDieAnim = true;
	private PlayerAttack m_playerAttack;
	
	void Start () 
	{
		m_target = GameObject.FindGameObjectWithTag("Player").transform;
		m_playerAttack = (PlayerAttack)GameObject.FindObjectOfType(typeof(PlayerAttack));
		m_navMeshAgent = (NavMeshAgent)GetComponent<NavMeshAgent>();
		m_startPos = transform.position;
		
        m_animator = GetComponent<Animator>();
		
		m_healthBar = (Health)FindObjectOfType(typeof(Health));
	}
	
	void Update () 
	{
		if( m_isDead )
		{
			m_animator.SetBool("Slash", false);
        	m_animator.SetFloat("Speed", 0.0f );
			if( m_playDieAnim )
			{
				m_animator.SetBool("Dead", true);
				m_playDieAnim = false;
			}
			else
			{
				m_animator.SetBool("Dead", false);
			}
			m_navMeshAgent.Stop();
			return;
		}
		
		bool followingPlayer = false;
		
		if( (m_target.position-transform.position).sqrMagnitude < (m_followDist*m_followDist)
			&& false == m_playerAttack.m_isDead )
		{
			m_navMeshAgent.SetDestination( m_target.position );
			followingPlayer = true;
		}
		else
		{
			m_navMeshAgent.SetDestination( m_startPos );
		}
			
		bool isMoving = (m_navMeshAgent.pathEndPosition - transform.position).magnitude > 2.0f;
        m_animator.SetFloat("Speed", isMoving ? 1.0f : 0.0f );
		
		if( isMoving )
		{
			m_animator.SetBool("Slash", false);
		}
		// slaying player
		else if( followingPlayer )
		{
			// just stopped moving because we arrived at players pos
			if( m_isMoving )
			{
				m_animator.SetBool("Slash", true);
			}
			
			RaycastHit hit;
			//Debug.DrawRay( m_hand.position, m_hand.forward * 2.0f, Color.red );
        	if(Physics.Raycast(m_hand.position, m_hand.forward, out hit, 2.0f, (1<<8))
				|| Physics.Raycast(m_hand.position+m_hand.right*0.3f, m_hand.forward, out hit, 2.0f, (1<<8)))
			{
				if( m_lastHitTime + 1.0f < Time.time )
				{
					m_healthBar.ChangeHearts(-1);
					m_lastHitTime = Time.time;
				}
			}
		}
        if (true)
        {
            if (!audio.isPlaying)
            {
                audio.clip = footSteps;
                audio.Play();
                audio.loop = true;
            }

            // this line may not even be necessary

        }
        else
        {
            audio.clip = footSteps;
            audio.loop = false;
            //audio.PlayOneShot(footSteps);
        }
		m_isMoving = isMoving;
	}
}
