using UnityEngine;
using System.Collections;

public class OverlayMenu : MonoBehaviour {
	
	public Texture2D background;
	public Texture2D continueButton;
	public Texture2D menuButton;
    public Texture2D tryAgain;
	
	private PlayerAttack m_player;
	bool isActive;
	
	// Use this for initialization
	void Start () {
		m_player = (PlayerAttack)GameObject.FindObjectOfType(typeof(PlayerAttack));
		isActive = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Pause")) {
			ToggleActive();
		}
		
        if ( m_player.m_isDead )
        {
            if (Input.GetMouseButtonDown(0)) 
            {
            Vector3 mousePosition = Input.mousePosition;
            // invert y position as the coordinate system works the other direction than the one for drawing
            mousePosition.y = Screen.height - mousePosition.y;
            Rect continueButtonRect = new Rect((Screen.width - background.width) * 0.5f + 25, (Screen.height - background.height) * 0.5f + 32, tryAgain.width, tryAgain.height);
            if (continueButtonRect.Contains(mousePosition))
            {
                Application.LoadLevel(1);
            }
            Rect menuButtonRect = new Rect((Screen.width - background.width) * 0.5f + 25, (Screen.height - background.height) * 0.5f + menuButton.height + 32, menuButton.width, menuButton.height);
            Debug.Log(mousePosition);
            Debug.Log(menuButtonRect);
            if (menuButtonRect.Contains(mousePosition))
            {
                UnPause();
                Application.LoadLevel(0);
            }
			}
            
        }
		else if (isActive)
        {
			if (Input.GetMouseButtonDown(0)) 
            {
				Vector3 mousePosition = Input.mousePosition;
				// invert y position as the coordinate system works the other direction than the one for drawing
				mousePosition.y = Screen.height - mousePosition.y;
				Rect continueButtonRect =  new Rect((Screen.width-background.width) * 0.5f + 25, (Screen.height-background.height) * 0.5f + 32, continueButton.width, continueButton.height);
				if (continueButtonRect.Contains(mousePosition)) {
					ToggleActive();		
				}
				Rect menuButtonRect = new Rect((Screen.width-background.width) * 0.5f + 25, (Screen.height-background.height)*0.5f+menuButton.height+32, menuButton.width, menuButton.height);
				Debug.Log(mousePosition);
				Debug.Log(menuButtonRect);
				if (menuButtonRect.Contains(mousePosition)) {
					UnPause();
					Application.LoadLevel(0);
				}
			}
		}
	}
	
	void OnGUI() 
    {
		if ( m_player.m_isDead)
        {
            GUI.BeginGroup(new Rect((Screen.width - background.width) * 0.5f, (Screen.height - background.height) * 0.5f, background.width, background.height));
            GUI.DrawTexture(new Rect(0, 0, background.width, background.height), background);
            GUI.DrawTexture(new Rect(25, 32, tryAgain.width, tryAgain.height), tryAgain);
            GUI.DrawTexture(new Rect(25, background.height - menuButton.height - 32, menuButton.width, menuButton.height), menuButton);
            GUI.EndGroup();
        }
		else if (isActive)
        {
			GUI.BeginGroup(new Rect((Screen.width-background.width) * 0.5f, (Screen.height-background.height) * 0.5f, background.width, background.height));
			GUI.DrawTexture(new Rect(0, 0, background.width, background.height), background);
			GUI.DrawTexture(new Rect(25, 32, continueButton.width, continueButton.height), continueButton);
			GUI.DrawTexture(new Rect(25, background.height-menuButton.height-32, menuButton.width, menuButton.height), menuButton);
			GUI.EndGroup();
		}
        
	}
	
	void ToggleActive() {
		if (isActive) {
			UnPause();
		} else {
			Pause();
		}
		isActive = !isActive;	
	}
	
	void Pause() {
		Time.timeScale = 0;
	}
	
	
	void UnPause() {
		Time.timeScale = 1;
	}
	
}
