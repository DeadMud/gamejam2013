using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
	public int m_zStep = 9;
	public int m_xStep = 16;
	
    private Transform m_player;
	
	
	
	void Start()
	{
		m_player = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	void Update()
	{
		float x = (((int)(m_player.position.x+8.0f)) / m_xStep) * m_xStep;
		float z = (((int)(m_player.position.z-5.0f)) / m_zStep) * m_zStep;
		transform.position = new Vector3(x, transform.position.y, z );
	}
}
