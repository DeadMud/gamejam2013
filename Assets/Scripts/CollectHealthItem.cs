using UnityEngine;
using System.Collections;

public class CollectHealthItem : MonoBehaviour {
	
	void OnTriggerEnter() {
		Health healthBar = (Health)FindObjectOfType(typeof(Health));
		if (healthBar.CurrentNumberOfHearts() < healthBar.heartNumber) {
			healthBar.ChangeHearts(1);
			Destroy(gameObject);
		}
	}
	
}