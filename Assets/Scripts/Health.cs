using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

    public Texture2D heart;
    public int heartNumber = 3;
    public float currentHeart;
    public float heartPiece;

	// Use this for initialization
	void Start () 
    {
        currentHeart = heart.width;
	}
	
	// Update is called once per frame
	void Update () 
    {
        
		if (Input.GetKeyDown(KeyCode.F))
        {
            this.ChangeHearts(-0.5f);
        }
        
	}

    void OnGUI() 
    {
        heartPiece = (heart.width) / 3;
        //currentHeart = heartPiece * heartNumber;

        {
            GUI.BeginGroup(new Rect(Screen.width * 0.05f, Screen.height * 0.03f, currentHeart, heart.height));
            GUI.DrawTexture(new Rect(0, 0, heart.width, heart.height), heart);
            GUI.EndGroup();
        }
    }
	
	public float CurrentNumberOfHearts() {
		return (float) (currentHeart / heartPiece);
	}
	
	public void ChangeHearts(float healthGained) {
		currentHeart += healthGained * heartPiece;
		currentHeart = Mathf.Min(currentHeart, heartNumber * heartPiece);
		currentHeart = Mathf.Max(currentHeart, 0);
	}
}
