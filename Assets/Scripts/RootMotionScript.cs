using UnityEngine;
using System.Collections;



public class RootMotionScript : MonoBehaviour
{

    protected Animator animator;
    public AudioClip footSteps;

    public float DirectionDampTime = .25f;
	private Health m_health;

    void Start()
    {
        animator = GetComponent<Animator>();
		m_health = (Health)GameObject.FindObjectOfType(typeof(Health));
    }

    void Update()
    {
        if (animator)
        {
            //get the current state
            AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
			
			bool playerDead = (5.0f > m_health.currentHeart);
			
			if( playerDead )
			{
				animator.SetFloat("Speed", 0.0f );
	            audio.loop = false;
			}
			else
			{
				
	            if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
	            {
	                if(!audio.isPlaying)
	                {
	                    audio.clip = footSteps;
						if( false == audio.isPlaying )
	                    	audio.Play();
	                    audio.loop = true;
	                }
	                
	                 // this line may not even be necessary
	                    
	            }
	            else
	            {
	                audio.clip = footSteps;
	                audio.loop = false;
	                //audio.PlayOneShot(footSteps);
	            }
	            //else
	            //{
	            //    audio.clip = footSteps;
	            //    audio.Stop();
	            //}
	
	            float h = Input.GetAxis("Horizontal");
	            float v = Input.GetAxis("Vertical");
	
	            //set event parameters based on user input
	            animator.SetFloat("Speed", (animator.GetBool("Slash")||playerDead) ? 0.0f : (h * h + v * v));
	            //animator.SetFloat("Direction", v, DirectionDampTime, Time.deltaTime);
			}
        }
        
        
    }
}
