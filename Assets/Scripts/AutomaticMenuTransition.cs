using UnityEngine;
using System.Collections;

public class AutomaticMenuTransition : MonoBehaviour {
	
	public float numberOfSecondsTillSwitch = 3.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	void Update() {
		numberOfSecondsTillSwitch -= Time.deltaTime;
		if (numberOfSecondsTillSwitch <= 0) {
			Application.LoadLevel(0);	
		}
	}
	
}
