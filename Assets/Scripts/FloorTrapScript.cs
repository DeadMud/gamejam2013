using UnityEngine;
using System.Collections;

public class FloorTrapScript : MonoBehaviour {
	
	bool isActive;
	// keeps track whether the trap did already damage within the current active phase, so that the trap will not drain all the hearts in one active phase
	bool didDamage;
	public float timeInactive = 2.0f;
	public float timeActive = 2.0f;
	
	float timePassedSinceLastStateChange;
	
	// Use this for initialization
	void Start () {
		isActive = false;
		didDamage = false;
		//transform.renderer.material.color = Color.blue;
		timePassedSinceLastStateChange = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
		timePassedSinceLastStateChange += Time.deltaTime;
		if (isActive && timePassedSinceLastStateChange >= timeActive) {
			isActive = false;
			transform.renderer.material.color = Color.gray;
			timePassedSinceLastStateChange = 0;
			didDamage = false;
			transform.localRotation = Quaternion.Euler ( 0.0f, 0.0f, 0.0f );
		}
		if (!isActive && timePassedSinceLastStateChange >= timeInactive) {
			isActive = true;
			transform.renderer.material.color = Color.red;
			timePassedSinceLastStateChange = 0;
			transform.localRotation = Quaternion.Euler ( 0.0f, 0.0f, 180.0f );
		}
	}
	
	void OnTriggerStay() {
		if (isActive && !didDamage) {
			Health healthBar = (Health)FindObjectOfType(typeof(Health));
			healthBar.ChangeHearts(-1);
			didDamage = true;
		}
	}
	
}
