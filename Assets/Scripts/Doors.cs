using UnityEngine;
using System.Collections;

public class Doors : MonoBehaviour {

    public GameObject door;
    public GameObject door2;
    public GameObject door3;
    public GameObject door4;


	// Use this for initialization
	void Start () 
    {
        
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            door.SetActive(false);

            if (door2 != null) 
            {
                door2.SetActive(false);
            }

            if (door3 != null)
            {
                door3.SetActive(false);
            }

            if (door4 != null)
            {
                door4.SetActive(false);
            }
        }	
	}
}
