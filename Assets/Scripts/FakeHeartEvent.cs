using UnityEngine;
using System.Collections;

public class FakeHeartEvent : MonoBehaviour {
	
	public AudioClip popSound;
	
	void OnTriggerEnter() {
		AudioSource.PlayClipAtPoint(popSound, gameObject.transform.position);
		Destroy(gameObject);
	}
	
}