using UnityEngine;
using System.Collections;

public class HeartBeatSound : MonoBehaviour {
	
	GameObject[] hearts;
	GameObject player;
	float closestDistance;
	
	private float m_playSoundTime = 0.0f;
	private float m_playSound2Time = 0.0f;
	
		VisualHeartBeat visualHeartBeat;
	
	// Use this for initialization
	void Start () {
		Debug.Log("Skript HeartBeatSound");
		hearts = GameObject.FindGameObjectsWithTag("Heart");
		player = GameObject.FindGameObjectWithTag("Player");
		visualHeartBeat = (VisualHeartBeat)FindObjectOfType(typeof(VisualHeartBeat));
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 playerPosition = player.transform.position;
		closestDistance = 5000; //float.infinity
		for (int i = 0; i < hearts.Length; i++) {
			
			if( null == hearts[i] )
				continue;
			
			GameObject heart = hearts[i];
			Vector3 heartPosition = heart.transform.position;
			float distance = Vector3.Distance (playerPosition, heartPosition);
			Debug.Log("distance " + distance);
			closestDistance = Mathf.Min(closestDistance, distance);
		}
		//float heartBeatSpeed = Mathf.Min((closestDistance+1.0f) / 9, 3.0f);
		float heartBeatSpeed = (Mathf.Clamp( closestDistance, 0.1f, 30.0f ) / 30.0f) * 3.0f;
		
		if( m_playSoundTime < Time.time )
		{
			audio.Play();
			m_playSoundTime = Time.time + heartBeatSpeed;
			m_playSound2Time = Time.time + heartBeatSpeed * 0.3f;
			
			visualHeartBeat.m_isBig = true;
		}
		if( m_playSound2Time < Time.time )
		{
			audio.Play();
			m_playSound2Time = Time.time + 1000.0f; 
			
			visualHeartBeat.m_isBig = false;
		}
	}
}
