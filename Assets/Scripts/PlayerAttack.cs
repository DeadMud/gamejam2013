using UnityEngine;
using System.Collections;

public class PlayerAttack : MonoBehaviour
{
	public float m_comboDist = 4.0f;
	public float m_attackDur = 0.8f;
	public GUITexture[] m_comboGTexs;
	private Enemy[] m_enemies;
	private Enemy m_enemyToAttack;
	
	private int m_comboStage = 0; // 0, 1, 2, kill
	private int[] m_comboKeys;
    private Animator m_animator;
	private ThirdPersonController m_controller;
	
	private float m_attackTime = 0.0f;
	private Health m_health;
	public bool m_isDead = false;
	
	void Start()
	{
		m_health = (Health)GameObject.FindObjectOfType(typeof(Health));
		m_enemies = (Enemy[])GameObject.FindObjectsOfType(typeof(Enemy));
		m_comboKeys = new int[3];
        m_controller = transform.parent.GetComponent<ThirdPersonController>();
        m_animator = GetComponent<Animator>();
	}
	
	void FixedUpdate()
	{
		Enemy enemyToAttack = null;
		
		float nearestDist = 9999.0f;
		foreach( Enemy e in m_enemies )
		{
			if( null == e )
				continue;
			
			float sqrMag = (transform.position-e.transform.position).sqrMagnitude;
			if( sqrMag < nearestDist )
			{
				if( sqrMag < m_comboDist*m_comboDist )
				{
					enemyToAttack = e;
					if( e != m_enemyToAttack )
					{
						ResetCombo();
					}
				}
				nearestDist = sqrMag;
			}
		}
		
		m_enemyToAttack = enemyToAttack;
	}
	
	void LateUpdate()
	{
		if( m_enemyToAttack && false == m_enemyToAttack.m_isDead && false == m_isDead)
		{
			Vector3 lookAt = new Vector3( 	m_enemyToAttack.transform.position.x, 
											transform.position.y,
											m_enemyToAttack.transform.position.z );
			transform.LookAt( lookAt );
		}
		else if( false == m_animator.GetBool("Slash" ) )
		{
			transform.localRotation = Quaternion.identity;
		}
	}
	
	void Update()
	{
		
		if( 5.0f > m_health.currentHeart && false == m_isDead )
		{
			m_controller.enabled = false;
			m_isDead = true;
			
			m_animator.SetFloat("Speed", 0.0f );
			m_animator.SetBool("Slash", false);
			m_animator.SetBool("Dead", true);
			
			Debug.Log ("DIE PLAYER DIE");
			
			for( int i=0; i<m_comboGTexs.Length; ++i )
			{
				m_comboGTexs[i].enabled = false;
			}
		}
		else
		{
			//m_animator.SetBool("Dead", false);
			
		}
		
		if( false == m_isDead )
		{
			if( m_attackTime + m_attackDur < Time.time && m_attackTime > 0.0f )
			{
				m_animator.SetBool("Slash", false);
				m_controller.enabled = true;
				
				m_attackTime = 0.0f;
			}
			
			/*if( m_animator.GetBool("Slash" ) )
			{
				transform.LookAt( lookAt );
			}*/
			
			if( null != m_enemyToAttack && true != m_enemyToAttack.m_isDead )
			{
				//if( Input.GetKeyDown( "Combo"+m_comboKeys[m_comboStage] ){}
				for( int i=0; i<4; ++i )
				{
					if( Input.GetButtonDown( "Combo"+i ) )
					{
						// success! next stage / kill
						if( i == m_comboKeys[m_comboStage] )
						{
							m_animator.SetBool("Slash", true);
							m_attackTime = Time.time;
							m_controller.enabled = false;
							
							// kill it!
							if( m_comboKeys.Length-1 == m_comboStage )
							{
								//Destroy(m_enemyToAttack.gameObject);
								m_enemyToAttack.m_isDead = true;
							}
							else
							{
								++m_comboStage;	
							}
						}
						// fail start over
						else
						{
							ResetCombo();
						}
					}
				}
				
				for( int i=0; i<m_comboGTexs.Length; ++i )
				{
					m_comboGTexs[i].enabled = (i == m_comboKeys[m_comboStage]);
				}
			}
			// reset
			else
			{
				for( int i=0; i<m_comboGTexs.Length; ++i )
				{
					m_comboGTexs[i].enabled = false;
				}
				ResetCombo();
			}
		}
	}
	
	void ResetCombo()
	{
		//Debug.Log ("ResetCombo");
		
		m_comboStage = 0;
		
		for( int i=0; i<m_comboKeys.Length; ++i )
		{
			// 4 different arrow keys to press as combo
			m_comboKeys[i] = Random.Range( 0, 4 ); // 4 is exclusive
			if(i>0)
			{
				while(m_comboKeys[i] == m_comboKeys[i-1])
				{
					m_comboKeys[i] = Random.Range( 0, 4 );
				}
			}
		}
	}
	
}
