using UnityEngine;
using System.Collections;

public class CameraSwitch : MonoBehaviour {

    //public GameObject target;
    public GameObject ObjectToCheck;
    public GameObject pivotOfRoom;
    public GameObject camera;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    void OnTriggerEnter(Collider col) 
    {
        if (col.gameObject == ObjectToCheck)
        {
            camera.transform.position = new Vector3(pivotOfRoom.transform.position.x, camera.transform.position.y, pivotOfRoom.transform.position.z);
        }
    }
}
