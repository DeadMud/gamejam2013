using UnityEngine;
using System.Collections;

public class MenuMovement : MonoBehaviour {

    int delta = 10;
    float moveStep = 1f;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void OnMouseEnter() 
    {
        //if (delta > 0)
        {
            gameObject.transform.Translate(0, 0, -moveStep);
            //delta--;
        }
    }

    void OnMouseExit()
    {
        //delta = 5;
        gameObject.transform.Translate(0, 0, moveStep);
    }
}
