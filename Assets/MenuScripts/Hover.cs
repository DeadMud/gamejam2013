using UnityEngine;
using System.Collections;

public class Hover : MonoBehaviour {

    public string levelToLoad;
    public AudioClip Soundhover, beep;
    public bool QuitButton = false;
    public bool LoadLevel = true;

    
    void OnMouseEnter() 
    {
        audio.PlayOneShot(Soundhover);
    }

    void OnMouseUp() 
    {
        audio.PlayOneShot(beep);
        if (LoadLevel)
        {
            Application.LoadLevel(1);
        }
        if (QuitButton)
        {
            Application.Quit();
        }
        
        //yield new WaitForSeconds(0.35f);
    }

    void OnMouseExit() 
    {
        //audio.PlayOneShot(Soundhover);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
